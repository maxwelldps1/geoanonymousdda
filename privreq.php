<?php
require_once("bitmix.php");
require_once("dbio.php");
$y = $_FILES['y'];
$z = $_POST['z'];

# its reported that a file upload thats too large also erases post vars
if ($_FILES['y']['size']===0 || $z === '' || !is_numeric($z)) echo "";
set_time_limit(2);
$hostname = "localhost";
$user = "default";
$password = "password";
$database = "USERDATABASE";
$authdb="AUTH";
#check the public key
$keyckcon = dbconnect($hostname,$user,$password,$authdb);
if($keyckcon != null){
    #check the public key against a valid connection list
	error_log("checking for public key: ".$z,0);
    if ( !($keyckcon->query('SELECT hash FROM CONNECTIONS WHERE clientkey='.$z)->fetch(PDO::FETCH_NUM)) ) echo "";
	
} else echo "";
$packetgram=file($_FILES['y']['tmp_name'],FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
$packsize=strlen(implode($packetgram));
error_log("decoded size: ".$packsize);
# the loaded file data is a string type, load it into a byte buffer
	$fixed_file=array_combine(range(0,$packsize-1),unpack("C*",$packetgram[0]));
$dest=encode($fixed_file,0);
$strpacket='';
for($i=0;$i<$packsize;$i++) $strpacket.=chr($dest[$i]);
error_log($strpacket);
$packetarray=json_decode($strpacket,true);

if(is_null($packetarray)) echo "";
#save the first 2 entries by name
$pk=$packetarray['pk'];
$callname=$packetarray['callname'];

#advance the json pointer past the first two entries
for($i=0;$i<2;$i++){
	$itmpnt=strpos($strpacket,',');
	$strpacket=substr($strpacket,$itmpnt,strlen($strpacket));
	# overwrite the delimeter ',' at the beginning of the ltrim'd string
	$strpacket[0]='{';	
}

#error_log($strpacket);

$packetarray=json_decode($strpacket,true);

$callargs="";
while($entry=current($packetarray)){
	if(is_null($entry) || ord($entry)==0) $callargs.='0';
    else $callargs.=$entry;
    if($entry!=array_key_last($packetarray)) $callargs.=',';
    next($packetarray);
}

$connection = dbconnect($hostname,$user,$password,$database);
if($connection != null){
    #answerset should be in json format, or null
    $strresponse=dbcall($connection, $pk, $callname.'('.$strpacket.')',1);
    if(is_null($strresponse) || $strresponse=='') echo "";
	# convert a string into a byte array
	$strdata=unpack("C*",stripslashes($strresponse));
	# shift the array index to start at zero, bitmix encode, then uu-encode / hex-encode
	// UNUSED HEX-ENCODING
	//$msggram=implode('',array_map("dechex",(encode(array_combine(range(0,count($strdata)-1),$strdata)))));
	$msggram=uuencode(encode(array_combine(range(0,count($strdata)-1),$strdata)));
	error_log("sending uu encoded packet, length: ".strlen($msggram).", ".$msggram);
	
	if(is_null($msggram)) echo "";
	else echo $msggram;
	
} else echo "";

// ------------------------------------------------------------------
// PHP Version 4
// ------------------------------------------------------------------
// Copyright (c) 1997-2004 The PHP Group
// ------------------------------------------------------------------
// This source file is subject to version 3.0 of the PHP license,
// that is bundled with this package in the file LICENSE, and is
// available at through the world-wide-web at
// http://www.php.net/license/3_0.txt.
// If you did not receive a copy of the PHP license and are unable to
// obtain it through the world-wide-web, please send a note to
// license@php.net so we can mail you a copy immediately.
/**
 * @category    PHP
 * @package     PHP_Compat
 * @link        http://php.net/function.convert_uuencode
 * @author      Michael Wallner <mike@php.net>
 * @author      Aidan Lister <aidan@php.net>
 * @author      Brandon Hill <maxwelldps@gmx.com>
 * @version     $Revision: 1.8 $
 * @since       PHP 5
 * @require     PHP 4.0.0 (user_error)
 * params: a byte array of the data to be encoded.
 * returns: a uu-encoded string.
 */
function uuencode($data){
	if(!is_array($data)) return "";
	# repack the data array into an array of 45 bytes per iteration
	# c is the length of the array, 45 or less
	$datachunk=array_chunk($data,45);
	$u = 0;
	$encoded = '';
	$i=0; #uuencoded line number
	while (array_key_exists($i,$datachunk) && $c = count($datachunk[$i])) {
		$bytes=$datachunk[$i++];
		$u += 45;
		# adds the line (length) header
		$encoded .= pack('c', $c + 0x20);// add dec:32, white space, to c

		# make the 45 byte array divisible by 3
		while ($c % 3) {
			$bytes[++$c] = 0;
		}

		foreach (array_chunk($bytes, 3) as $b) {
			$b0 = ($b[0] & 0xFC) >> 2;
			$b1 = (($b[0] & 0x03) << 4) + (($b[1] & 0xF0) >> 4);
			$b2 = (($b[1] & 0x0F) << 2) + (($b[2] & 0xC0) >> 6);
			$b3 = $b[2] & 0x3F;

			$b0 = $b0 ? $b0 + 0x20 : 0x60;
			$b1 = $b1 ? $b1 + 0x20 : 0x60;
			$b2 = $b2 ? $b2 + 0x20 : 0x60;
			$b3 = $b3 ? $b3 + 0x20 : 0x60;

			$encoded .= pack('c*', $b0, $b1, $b2, $b3);
		}

		$encoded .= "\n";
	}

	// Add termination characters
	$encoded .= "\x60\n";

	return $encoded;
}


?>

