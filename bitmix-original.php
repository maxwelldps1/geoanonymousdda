<?php
# php port of andcode: an alternate binary encoding.
function encode($src_pnt, $encode=1){
	$len=count($src_pnt);
	$msb_val=array(192,48,12,3);
	$dest=array_fill(0,$len,0);
	$bits_out=$byte_out=$bitpair_cnt=0;
	$bit1 = $bit2 = $bits = $byte = 0;
	$i=0;
	while($i<$len){
		$byte=$src_pnt[$i];
		while($bitpair_cnt<4){
			$bits = $byte & ($msb_val[$bitpair_cnt]);
			$byte-=$bits;
			$bits >>= 8 - (($bitpair_cnt+1) * 2);
			if($bits>1){
				$bits -= 2;
				$bit2 = 1;
			} else $bit2 = 0;
			$bit1=$bits;	
			if($encode){
				if($bit1==$bit2) $bits_out=(!$bit2)?3:2;
				else if($bit2!=0) $bits_out=1;
			} else {
				if($bit2){
					if(!$bit1) $bits_out=3;
				} else $bits_out=($bit1)?2:1;	
			}
			$byte_out += $bits_out;
			$bits_out=0;
			if ($bitpair_cnt<3) $byte_out <<= 2;
			$bitpair_cnt++;			
		}
		$dest[$i]=$byte_out;
		$bitpair_cnt=$byte_out=0;
		$i++;
	}
	return $dest;
}
?>
