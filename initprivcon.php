<?php
require_once("dbio.php");
#initialize android connection (longitude, latitude)
#get the UTC from the packet
date_default_timezone_set('UTC');
$y = date('Y-m-d H:i:s');

$z = $_POST['z'];
if ($z === '' || strlen($z) < 32 || !is_numeric($z)) echo "";
# the locations must be 17(10+7) digits in length(precision returned from webUI), plus the decimal
# 000.0000000 + additional 7, 14 precision digits
set_time_limit(2);
$hostname = "localhost";
$user = "default";
$password = "password";
$database = "AUTH";

$connection = dbconnect($hostname,$user,$password,$database);

if($connection!=null){
    $anset = dbcall($connection, $z, "GET_HASH(".$z.", ".strval(strlen($z)).")",1);
    if($anset!=null) echo json_decode($anset)[0];
    else echo "";
} else echo "";
?>

