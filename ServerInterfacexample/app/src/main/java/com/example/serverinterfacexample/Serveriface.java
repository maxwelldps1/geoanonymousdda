package com.example.serverinterfacexample;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import dalvik.system.InMemoryDexClassLoader;

public class Serveriface {

    private HttpURLConnection connection;
    public static enum statusRange{FAILED,READY,OK,DONE};
    private int status= statusRange.OK.ordinal();
    private Class thisMixer = null;
    private Method encoder, decoder=null;

    /*
     * During runtime, verify the connection is ok by testing
     *      (ifaceInstance.GetStatus() == Serveriface.statusRange.OK)
     */
    public int GetStatus(){
        return status;
    }

    public Serveriface(Context context) throws InvocationTargetException, IllegalAccessException, InstantiationException {
        /*
         * Instantiate the msg encoder
         * */
        try {
            InputStream stream = context.getResources().openRawResource(R.raw.bitmix);
            byte[] buff = new byte[stream.available()];
            stream.read(buff, 0, stream.available());
            stream.close();

            ByteBuffer residentDex = ByteBuffer.wrap(buff);

            InMemoryDexClassLoader loader = new InMemoryDexClassLoader(residentDex, ClassLoader.getSystemClassLoader());
            thisMixer = loader.loadClass("com.example.serverinterfacexample.Bitmix");
            this.decoder=thisMixer.getMethods()[0];
            this.encoder=thisMixer.getMethods()[1];


            Log.d("FileIO ", "finished loading dex file");
        } catch (IOException ex) {
            this.encoder=this.decoder=null;
            throw new RuntimeException(ex);
            //dex loading failed
        } catch (ClassNotFoundException ex) {
            this.encoder=this.decoder=null;
            throw new RuntimeException(ex);
            // dex class error
        }
    }
    private ByteBuffer uuBuff;

    private HttpURLConnection GetHttpCon(String strUrl) {
        HttpURLConnection client = null;
        try {
            URL url = new URL(strUrl);
            client = (HttpURLConnection) url.openConnection();
            client.setConnectTimeout(2000);
            client.setReadTimeout(2000);
            this.status= statusRange.READY.ordinal();
        } catch (Exception e) {
            //debug.log(e);
            this.status= statusRange.FAILED.ordinal();
            client=null;
        }
        return this.connection=client;
    }
    public String GetPrivKey(String pubkey) {
        // debug address to the emulating machine
        HttpURLConnection con = GetHttpCon("http://10.0.2.2/initprivcon.php");
        String privkey = "";
        if (con != null && !pubkey.isEmpty()) {
            try {
                Log.d("ServerIO", "creating the server msg");
                StringBuilder pubKeyMsg = new StringBuilder(URLEncoder.encode("z", "UTF-8") + "=").append(URLEncoder.encode(pubkey, "UTF-8"));
                con.setRequestMethod("POST");
                con.setRequestProperty("Connection", "Keep-Alive");
                con.setRequestProperty("Cache-Control", "no-cache");
                con.setRequestProperty("Accept-Charset", "UTF-8");
                con.setDoInput(true);
                con.setDoOutput(true);
                //non-buffered transfer
                con.setFixedLengthStreamingMode(pubKeyMsg.length());

                Log.d("ServerIO", "preparing to open the connection");
                con.connect();

                DataOutputStream outPost = new DataOutputStream(con.getOutputStream());

                outPost.write(pubKeyMsg.toString().getBytes());

                outPost.flush();
                outPost.close();
                if (con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    Log.d("ServerIO", "sent the server msg");
                    BufferedReader reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
                    privkey=reader.readLine();
                } // if the response is OK
                con.disconnect();
            } catch (ProtocolException e) {
                throw new RuntimeException(e);
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return privkey;
    }


    // ------------------------------------------------------------------
// PHP Version 4
// ------------------------------------------------------------------
// Copyright (c) 1997-2004 The PHP Group
// ------------------------------------------------------------------
// This source file is subject to version 3.0 of the PHP license,
// that is bundled with this package in the file LICENSE, and is
// available at through the world-wide-web at
// http://www.php.net/license/3_0.txt.
// If you did not receive a copy of the PHP license and are unable to
// obtain it through the world-wide-web, please send a note to
// license@php.net so we can mail you a copy immediately.
// ------------------------------------------------------------------
// Authors: Michael Wallner <mike@php.net>
//          Aidan Lister <aidan@php.net>
// ------------------------------------------------------------------
    /**
     *
     * @category    PHP
     * @package     PHP_Compat
     * @link        http://php.net/function.convert_uudecode
     * @author      Michael Wallner <mike@php.net>
     * @author      Aidan Lister <aidan@php.net>
     * @author      Brandon Hill <maxwelldps@gmx.com>
     * @version     $Revision: 1.8 $
     * @since       PHP 5
     * @require     PHP 4.0.0 (user_error)
     */
    private ByteBuffer uuDecode(BufferedReader rdr){
        List<Byte> decoded = new ArrayList<Byte>();
        try {
            for (String line: rdr.lines().toArray(size->new String[size])){
                if(line.isEmpty()) continue;
                ArrayList<Byte> bytes=new ArrayList<>();
                // add each byte of the string after the first
                for(char c: line.trim().substring(1).toCharArray()) bytes.add((byte)c);
                int l = bytes.size();
                // pad the line until its divisible by 4
                while (l<4 && l%4>0) {bytes.add((byte)0);++l;}
                // the number of 4 byte segments
                l/=4;
                for (int byte0,byte1,byte2,byte3,i=0;i<l;i++){
                    byte0=bytes.get(i*4);
                    byte1=bytes.get(i*4+1);
                    byte2=bytes.get(i*4+2);
                    byte3=bytes.get(i*4+3);
                    int b0 = byte0 == 0x60 ? 0 : byte0 - 0x20;
                    int b1 = byte1 == 0x60 ? 0 : byte1 - 0x20;
                    int b2 = byte2 == 0x60 ? 0 : byte2 - 0x20;
                    int b3 = byte3 == 0x60 ? 0 : byte3 - 0x20;
                    b0 <<= 2;
                    b0 |= (b1 >> 4) & 0x03;
                    b1 <<= 4;
                    b1 |= (b2 >> 2) & 0x0F;
                    b2 <<= 6;
                    b2 |= b3 & 0x3F;
                    decoded.add((byte)b0);
                    decoded.add((byte)b1);
                    decoded.add((byte)b2);
                }
            }
            // strip excess terminators from the decoded stream
            int endsub=decoded.size()-1;
            while(endsub>0 && decoded.get(endsub).byteValue()==0) {decoded.remove(endsub); endsub--;}
            uuBuff=ByteBuffer.allocate(decoded.size());
            for(Byte db: decoded.toArray(new Byte [decoded.size()])) uuBuff.put(db.byteValue());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

        return uuBuff;
    }

    public byte[] ServerTransaction(String pubkey, byte[] datagram) {
        byte[] rtn = null;
        // make a new connection to the server tunnel
        HttpURLConnection con=GetHttpCon("http://10.0.2.2/privreq.php?");
        if (con != null && !pubkey.isEmpty() && datagram != null) {
            String packdiv="@~*#";
            String packend="\r\n--"+packdiv+"\r\n";
            StringBuilder postMsg = new StringBuilder();
            postMsg.append("--"+packdiv+"\r\nContent-Disposition: form-data; name=\"z\"\r\n" );
            postMsg.append("Content-Type:text/plain;charset=UTF-8\r\n\r\n");
            postMsg.append(pubkey+"\r\n");

            postMsg.append("--"+packdiv+"\r\nContent-Disposition: form-data; name=\"y\";filename=\"y\"\r\n" );
            postMsg.append("Content-Type:application/octet-stream\r\n");
            postMsg.append("Content-Transfer-Encoding:binary\r\n");
            //postMsg.append("Transfer-Encoding:chunked\r\n"+String.valueOf(datagram.length)+"\r\n");
            postMsg.append("\r\n");

            try {
                con.setDoInput(true);
                con.setDoOutput(true);
                con.setUseCaches(false);
                con.setRequestMethod("POST");
                con.setRequestProperty("Connection", "Keep-Alive");
                con.setRequestProperty("ENCTYPE", "multipart/form-data");
                con.setRequestProperty("Content-Type","multipart/form-data;boundary="+packdiv);
                //non-buffered transfer
                //con.setFixedLengthStreamingMode(postMsg.length()+datagram.toString().length()+packend.length());
                con.connect();
                DataOutputStream outPost = new DataOutputStream(con.getOutputStream());
                outPost.write(postMsg.toString().getBytes());
                // post the datagram
                outPost.write(datagram);
                // post the datagram footer
                outPost.writeBytes(packend);
                // close the stream writer
                outPost.flush();
                outPost.close();

                if(con.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    /*
                    // UNUSED HEX-DECODING
                    // decode the response stream
                    BufferedReader rdr =new BufferedReader(new InputStreamReader(con.getInputStream()));
                    StringBuilder compoundResponse=new StringBuilder();
                    for (String line: rdr.lines().toArray(size->new String[size]))
                        compoundResponse.append(line);
                    Log.d("server input stream",compoundResponse.toString().trim());
                    byte[] bytes=new byte[compoundResponse.length()/2];
                    for(int i=0,s=0;i< compoundResponse.length();i+=2,s++)
                        bytes[s]=(byte)Integer.parseInt(compoundResponse.toString().substring(i,i+2),16);
                    // return the decoded hex string buffer
                    rtn=bytes;
                    */
                    uuDecode(new BufferedReader(new InputStreamReader(con.getInputStream())));
                    if(uuBuff!=null && uuBuff.capacity()>0) rtn=uuBuff.array();
                }
            } catch (ProtocolException e) {
                throw new RuntimeException(e);
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            } catch (IOException e) {
                Log.e("ServerIO",e.getMessage());
                throw new RuntimeException(e);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
            con.disconnect();
        }
        return rtn;
    }


    /*
     * connect to the server and get a private key using the public key
     */
    // replace with the users location-formatted string
    public JSONObject Call(String callName, JSONObject funArgs){
        JSONObject rtn=null;
        // get a random public key
        Random r = new Random(System.currentTimeMillis());
        int[] values=r.ints(32,1,10).toArray();
        StringBuilder keyBuilder=new StringBuilder();
        // add the ones digit of the new int string to the compound number
        for(int i=0;i<32;i++) keyBuilder.append(String.valueOf(values[i]));

        String publicKey = keyBuilder.toString();

        JSONObject jsonDatagram = new JSONObject();

        /*
         * prepend the message packet
         */

        try {
            // get the private key
            String privkey = GetPrivKey(publicKey);

            if(privkey.isEmpty()) throw new Exception("error getting private key");
            Log.d("Private Key",privkey);
            jsonDatagram.put("pk", privkey);
            jsonDatagram.put("callname", (Object) callName);
            //append the function arguments to the json packet
                Iterator<String> key=funArgs.keys();
                while(key.hasNext()){
                        String strkey=key.next();
                        Object value=funArgs.get(strkey);
                        jsonDatagram.put(strkey,value);
                }
        } catch (JSONException ex) {
            throw new RuntimeException(ex);
        } catch (Exception ex) {
            Log.e("ServerIO",ex.getMessage());
            throw new RuntimeException(ex);
        };


        /*
         * call the encoder method
         */
        int buflen=jsonDatagram.toString().length();
        Log.d("Buffer Allocate",String.valueOf(buflen)+" bytes");
        ByteBuffer encodedDatagram=ByteBuffer.allocate(buflen>10?buflen:10);
        // MAGIC NO-WRITE CODE, BUFFER SHOULD BE AT LEAST THIS SIZE
        encodedDatagram.put(ByteBuffer.wrap(new byte[]{'W','R','I','T','E','F','A','I','L','\0'}));
        byte[] response = null;

        try{
            Log.d(thisMixer.getDeclaredMethods()[1].getName(), "running encoder...");

            this.encoder.invoke(null, buflen, ByteBuffer.wrap(jsonDatagram.toString().getBytes(Charset.forName("UTF-8"))), encodedDatagram);

            if(new String("WRITEFAIL").contains(encodedDatagram.toString())) Log.e("DEXIO","encoding failed");
            else Log.d("DEXIO","done encoding");
        } catch(InvocationTargetException e){
            throw new RuntimeException(e);
        } catch(IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        /*
         * send an datagram to the server
         */
        if(encodedDatagram!=null) response = ServerTransaction(publicKey, encodedDatagram.array());
        // this is the first row of the sql answerset, if any
        if(response!=null){
            try {

                    String failTag="WRITEFAIL";
                    ByteBuffer failFlag= ByteBuffer.allocate(failTag.length());
                    encodedDatagram=ByteBuffer.allocate(response.length>failTag.length()?response.length:failTag.length());

                    this.encoder.invoke(null,failTag.length(),ByteBuffer.wrap(failTag.getBytes()),failFlag);
                Log.d("server response length: ", String.valueOf(response.length));

                Log.d(thisMixer.getDeclaredMethods()[0].getName(), "running decoder...");
                    this.decoder.invoke(null,response.length, ByteBuffer.wrap(response), encodedDatagram);
                    // if the flag or part of it matches
                    if(failFlag.toString().contains(CharBuffer.wrap(encodedDatagram.array().toString()))) Log.e("DEXIO","decoding failed");
                    else Log.d("DEXIO","done decoding");
                    // encodedDatagram now contains the decoded response
                    //Log.d("server response: ", new String(encodedDatagram.array(),StandardCharsets.US_ASCII));
                    // an empty string was passed through encoding
                    if(new String(encodedDatagram.array(),StandardCharsets.US_ASCII).contains(CharBuffer.wrap(new char[]{91,34,34,93,0}))) Log.d("ServerIO","empty response message");
                    // the answer string is enclosed in brackets, and quotes.... so remove them
                    else{
                        String jsonResult=new String(encodedDatagram.array(),StandardCharsets.US_ASCII).substring(2,encodedDatagram.capacity()-1);
                        rtn=new JSONObject(jsonResult);
                    }
                    // debug, print each byte of the decoded message
                    //for(int b=0,i=0;i<encodedDatagram.capacity();i++) Log.d("byte: ", String.valueOf(encodedDatagram.array()[i] & 0xFF));
            } catch (IllegalAccessException e) {
                throw new RuntimeException(e);
            } catch (InvocationTargetException e) {
                throw new RuntimeException(e);
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        } else Log.d("ServerIO","no response message");

        // the end
        return rtn;
    }

}//class
