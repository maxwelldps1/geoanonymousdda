package com.example.serverinterfacexample;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitAll().build());
        setContentView(R.layout.activity_main);



        Serveriface svrIface=null;
        try {
            svrIface = new Serveriface(getBaseContext());
        } catch (InvocationTargetException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        }


        JSONObject jsonDatagram = new JSONObject();

        /*
         * format the message packet
         */
        try

        {
            jsonDatagram.put("userid", (Object) "1234567890");
            jsonDatagram.put("name", (Object) "Ben Davis");
            jsonDatagram.put("password", (Object) "12345");
            jsonDatagram.put("address", (Object) "842 Cherry St.");
            jsonDatagram.put("registrationtime", (Object) "yy:mm:dd h:m:s");
            jsonDatagram.put("eventsadded", (Object) "0");
            jsonDatagram.put("eventsjoined", (Object) "0");
            jsonDatagram.put("lasteventjoined", (Object) "null");
            //add other params
        } catch(JSONException e){
            throw new RuntimeException(e);
        }
        if(svrIface!=null)jsonDatagram = svrIface.Call("SETREGISTEREDUSER",jsonDatagram);
        // the server returned the matching row from the database, without changes
        // or null if the insert succeeded
        if(jsonDatagram!=null) Log.d("Existing table row",jsonDatagram.toString());

        // give the old response to the garbage collector
        jsonDatagram=new JSONObject();
        try {
            jsonDatagram.put("userid", (Object) "1234567890");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        // only the userid (primary key) is required to get the table row
        if(svrIface!=null)jsonDatagram = svrIface.Call("GETREGISTEREDUSER",jsonDatagram);
        if(jsonDatagram!=null) Log.d("Retrieved table row",jsonDatagram.toString());

        //try to get a user that doesn't exist
        jsonDatagram=new JSONObject();
        try {
            jsonDatagram.put("userid", (Object) "1111111111");
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
        if(svrIface!=null)jsonDatagram = svrIface.Call("GETREGISTEREDUSER",jsonDatagram);
        if(jsonDatagram==null) Log.d("Retrieved table row","none, user doesn't exist");
    }
}