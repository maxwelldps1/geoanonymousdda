<?php

$contact_time = time();
$dbreqtimeout=2000;

mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);

function dbconnect($hostname,$username, $password,$datab){
    try{ $con=new PDO("mysql:host=".$hostname.";dbname=".$datab,$username,$password);}
    catch(PDOException $e){
        error_log($e->getMessage());
        #die($e);
    }
    # return the open connection ref or null
    return $con;
}

function getPDOtype($pdovalue){
	#get the param type
	if (ctype_alnum($pdovalue)){
		if (!ctype_alpha($pdovalue)){
			#search for a decimal point
			if(strstr($pdovalue,'.')){
			#type float TODO: LOB packing
			return PDO::PARAM_LOB;
			} else{
			# type int, check for over/underflowing int_size TODO: LOB type packing
			if($pdovalue > PHP_INT_MAX || $pdovalue < PHP_INT_MIN) return PDO::PARAM_STR;
			else return PDO::PARAM_INT;
			}
		} #else its a string
		else return PDO::PARAM_STR;
	}
	#type is binary
	else return PDO::PARAM_LOB;
}

function strloc($haystack,$needle,$off){
	$rtn=strpos($haystack,$needle,$off);
	if(!$rtn)
		if (substr($haystack,$off,1)==$needle) return 0;
		else return (-1);
	return $rtn;
}


# returns (-1) on not found; or the location of next comma, as long as it precedes a { or the end
function nextCallArg($strparams, $intstart){
	#test for string location zero
	
	$jsonpnt=strloc($strparams, '{', $intstart);
	$listpnt=strloc($strparams, ',', $intstart);
	if($listpnt!=(-1))
		if($jsonpnt!=(-1)) return($listpnt<$jsonpnt)?$listpnt:(-1);
		else return $listpnt;
	return (-1);
}

/*
 Accepts a PDO object from dbconnect(),
	the public key,
	a typical function call consisting of "FunctionName(argValue1,argValue2,etc)",
	and the number of expected returns; a sql procedure can return multiple values.

The number of function parameters can be up to 27, encoded as 'a' to 'z'.
The procedural return values can also range up to 27, and are encoded as 'Z' to 'A'

Returns null if nothing was returned from the DBMS, or the range of specified values in a json string.
*/

function dbcall($conref, $clientkey, $funheader, $numcols){
$rtn=null;
if ($conref!=null && strlen($clientkey)>0 && strlen($funheader)>0) try{
    $GLOBALS['contact_time']=time();
    
	error_log("header ".$funheader);
	
    #process the param list
    $plabel='a';
    $newfunhdr=substr($funheader,0,strpos($funheader,'(')+1);# string contains the name and leading '('
    $params=substr($funheader,strpos($funheader,'(')+1, strpos($funheader,')')-strpos($funheader,'(')-1);
    if ($params){# can be null or uninitialized
		
		# use a parallel array of string params and pdo::types
        $paramval=array();
        $paramtype=array();
		error_log("params ".$params);
		$paramstart=0;
		$paramend=strlen($params);
		$totalparams=0;
		
		//while($paramend!=(-1)){
			# add up individual params prior to a json obj, if any
			$paramend=nextCallArg($params, $paramstart);
			while($paramend!=(-1)){ #explicit function sentinal
					$paramval[]=substr($params,$paramstart,$paramend);
					$paramtype[]=getPDOtype(end($paramval));
					$totalparams++;
					error_log("adding before");
				$paramstart=$paramend+1;
				$paramend=nextCallArg($params, $paramstart);
			}
			
			# overlook json string arguments
			while(substr(trim( substr($params,$paramstart,strlen($params)-$paramstart)),0,1)=='{'){
				$paramend=strpos($params, '}', $paramstart);
				# critical error, no closing bracket found, or it follows an opening bracket
				if(!$paramend) break;
				# check for a comma after a closing brace
				else{
					# search for a comma seperated list between json objs
					$paramdelim=nextCallArg($params, $paramend+1);//strpos($params, ',', $paramend+1);
					if ( $paramdelim != (-1) ){ //&& substr(trim( substr($params,$paramend+1,strlen($params)) ),0,1)==','
						# accumulate any individual parameters between json objs
						while($paramdelim!=(-1)){
								$paramval[]=trim(substr($params,$paramstart,$paramend-$paramstart));
								$paramtype[]=getPDOtype(end($paramval));
								$totalparams++;
								error_log("adding between");
							$paramstart=$paramend=$paramdelim;
							$paramdelim=nextCallArg($params, $paramend+1);
						}// while
						# no comma after the obj, adding one json obj
					}else{
						error_log("adding json ".substr($params,$paramstart,$paramend+1));
						$paramval[]=substr($params,$paramstart,$paramend-$paramstart+1);
						$paramtype[]=PDO::PARAM_STR;
						$totalparams++;
					}
				}
			# advances past } or ,
			$paramstart=$paramend+1;
			} // while
			
			# add up individual params past json objs, if any
			$paramend=nextCallArg($params, $paramstart);
			while($paramend!=(-1)){
					$paramval[]=trim(substr($params,$paramstart,$paramend));
					$paramtype[]=getPDOtype(end($paramval));
					$totalparams++;
					error_log("adding after");
				$paramstart=$paramend+1;
				$paramend=nextCallArg($params, $paramstart);
			}
		//} // while the end != len
		# add the last, or only parameter
		if(trim(substr($params,$paramstart,strlen($params)-$paramstart))!=''){
			error_log("remaining param");
			$paramval[]=trim(substr($params,$paramstart,strlen($params)-$paramstart));
			$paramtype[]=getPDOtype(end($paramval));
			$totalparams++;
		}
		$i=0;
        while($i<$totalparams){
			# write-out the PDO parameter string
			$newfunhdr.=':'.$plabel;
			#if there is another input parameter
			if($i < $totalparams) $newfunhdr.=',';
            $plabel=chr(ord($plabel)+1);
            $i++;
        }// while
        #setup the answerset-vars
        $strtnvars='';
        $rlabel='Z';
        $i=0;
        while($i<$numcols){
            if($numcols&&$i<$numcols-1) $strtnvars.=',';
            $strtnvars.='@'.$rlabel;
            $rlabel=chr(ord($rlabel)-1);
            #if the output parameters underflow
            if($rlabel==ord('A')-1) break;
            $i++;
        }    

		#error_log('CALL '.$newfunhdr.$strtnvars.')');
		#begin the query process
        $callquery=$conref->prepare('CALL '.$newfunhdr.$strtnvars.')');
        
		#bind the input parameters
        $plabel='a';
        $i=0;
        while($i<$totalparams){
			error_log("sending: ".$paramval[$i]);
			$callquery->bindParam(':'.$plabel, $paramval[$i], $paramtype[$i]);
			$plabel=chr(ord($plabel)+1);
			# if the input params overflow
			if($plabel==ord('z')+1) break;
			$i++;
        }// while binding the params
        #end the query process
        $callquery->execute();
        $callquery->closeCursor();
    } // input checking the parameter
    #pack the answer set in json
    $rtn=json_encode($conref->query('SELECT '.$strtnvars)->fetch(PDO::FETCH_NUM));
} catch(PDOException $e){
    error_log($e->getMessage());
    #die($e);
}
# return something from the db or nothing
return $rtn;
}
?>
